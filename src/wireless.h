#ifndef LIGHTSWITCH_WIRELESS_H_
#define LIGHTSWITCH_WIRELESS_H_

#include <ESP8266WiFi.h>

namespace lightswitch
{
    class Wireless
    {
    private:
        const char *ssid_;
        const char *password_;
        
    public:
        Wireless(const char *ssid, const char *password = nullptr);
        void Connect();
    };
} // namespace lightswitch

#endif // LIGHTSWITCH_WIRELESS_H_