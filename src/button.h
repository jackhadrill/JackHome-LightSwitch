#ifndef LIGHTSWITCH_BUTTON_H_
#define LIGHTSWITCH_BUTTON_H_

namespace lightswitch
{
    class Button
    {
    public:
        Button(unsigned short int pin);
        void Setup();
        unsigned short int pin_;
        volatile bool pressed_;
    };
} // namespace lightswitch

#endif // LIGHTSWITCH_BUTTON_H_