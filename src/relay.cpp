#include "relay.h"

#include <Arduino.h>

namespace lightswitch
{
    Relay::Relay(unsigned short int pin)
        : pin_(pin),
          state_(false) {}

    void Relay::Setup()
    {
        pinMode(pin_, OUTPUT);
    }

    void Relay::Toggle()
    {
        state_ ? Relay::Off() : Relay::On();
    }

    void Relay::On()
    {
        state_ = true;
        digitalWrite(pin_, HIGH);
    }

    void Relay::Off()
    {
        state_ = false;
        digitalWrite(pin_, LOW);
    }
} // namespace relay
