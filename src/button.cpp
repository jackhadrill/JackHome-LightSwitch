#include "button.h"

#include <Arduino.h>

namespace lightswitch
{
    Button::Button(unsigned short int pin)
        : pin_(pin),
          pressed_(false) {}

    void Button::Setup()
    {
        pinMode(pin_, INPUT);
    }
} // namespace lightswitch
