#ifndef LIGHTSWITCH_RELAY_H_
#define LIGHTSWITCH_RELAY_H_

namespace lightswitch
{
    class Relay
    {
    private:
        unsigned short int pin_;
        bool state_;
        
    public:
        Relay(unsigned short int pin);
        void Setup();
        void Toggle();
        void On();
        void Off();
    };
} // namespace lightswitch

#endif // LIGHTSWITCH_RELAY_H_