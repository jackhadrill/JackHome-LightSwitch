#include "wireless.h"

#include <ESP8266WiFi.h>

namespace lightswitch
{
    Wireless::Wireless(const char *ssid, const char *password)
        : ssid_(ssid),
          password_(password) {}

    void Wireless::Connect()
    {
        WiFi.mode(WIFI_STA);
        password_ ? WiFi.begin(ssid_, password_) : WiFi.begin(ssid_);
    }
} // namespace lightswitch
